using System;
using Interop.QBFC16; // QuickBooks SDK reference

namespace QBDTest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Create a session manager object
                QBSessionManager sessionManager = new QBSessionManager();

                // Set up the connection parameters
                sessionManager.OpenConnection2("", "QBDIntegration", ENConnectionType.ctLocalQBD);
                sessionManager.BeginSession("", ENOpenMode.omDontCare);

                // Create a request processor object
                IMsgSetRequest requestSet = sessionManager.CreateMsgSetRequest("US", 13, 0);
                requestSet.Attributes.OnError = ENRqOnError.roeContinue;

                // Create a specific request
                ICustomerQuery customerQuery = requestSet.AppendCustomerQueryRq();

                // Customize the request parameters
                customerQuery.ORCustomerListQuery.CustomerListFilter.ORNameFilter.NameRangeFilter.FromName.SetValue("A");
                customerQuery.ORCustomerListQuery.CustomerListFilter.ORNameFilter.NameRangeFilter.ToName.SetValue("Z");

                // Send the request to QuickBooks
                IMsgSetResponse responseSet = sessionManager.DoRequests(requestSet);

                // Process the response
                if (responseSet.ResponseList.GetAt(0).StatusCode == 0)
                {
                    IResponse response = responseSet.ResponseList.GetAt(0);
                    ICustomerRetList customerList = (ICustomerRetList)response.Detail;

                    // Iterate through the customer list
                    for (int i = 0; i < customerList.Count; i++)
                    {
                        ICustomerRet customer = customerList.GetAt(i);
                        Console.WriteLine($"Customer Name: {customer.Name.GetValue()}");
                    }
                }
                else
                {
                    // Handle any errors
                    Console.WriteLine($"Error: {responseSet.ResponseList.GetAt(0).StatusMessage}");
                }

                // End the session and close the connection
                sessionManager.EndSession();
                sessionManager.CloseConnection();
            }
            catch (Exception ex)
            {
                // Handle any exceptions
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
        }
    }
}
